$(document).ready(function(){
    let hamburgerMenu = document.querySelector('.hamburger-icon');
    let fullPage = document.querySelector('body');
    let openMenu = document.querySelector('.open_menu');
    let closeMenu = document.querySelector('.close-icon');
     
    hamburgerMenu.addEventListener("click",function(){
    openMenu.style.display = 'flex';
    openMenu.style.top = '0';
    fullPage.style.overflow = 'hidden'
    });
    closeMenu.addEventListener("click",function(){
    openMenu.style.display= 'none';
    });
     
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:52,
    nav:true,
    item:3,
    autoWidth : true,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsive : {
    0: {
    item:1,
    margin:35,
    },
    480 : {
    item :1,
    margin:35,
    },
    768 : {
    item:2,
    margin:35,
    },
    960 : {
    item : 2,
    },
    }
    });
     
    $(".image-gallery").lightGallery({
    mode: 'lg-slide',
    loop :false,
    getCaptionFromTitleOrAlt:false,
    download:false,
    counter:false,
    zoom:false,
    share:false,
    rotate:false,
    fullScreen:false,
    thumbnail:false,
    autoplayControls:false,
    });
    });